import axios from "axios"
class FormService {

    constructor() {
        this.url = "https://api.bd2g.com/v1"
    }

    // return list of differents status
    getCompanyStatus = () => {
        return ["SARL", "EURL", "EI", "SASU"]
    }

    sendStatut = (object) => {
        try {
            axios.post(`${this.url}/soanstatut`, { object })
        } catch (e) {
            console.log(e)
        }
    }


    sendStep2 = (object) => {
        try {
            axios.post(`${this.url}/soancompanyinfo`, { object })
        } catch (e) {
            console.log(e)
        }
    }

    sendStep3 = (object) => {
        try {
            axios.post(`${this.url}/soancompanyrepresentant`, { object })
        } catch (e) {
            console.log(e)
        }
    }

    getcountriesName = async () => {
        const res = await axios.get('https://restcountries.eu/rest/v2/all')
        return res.data
    }
}

export default FormService