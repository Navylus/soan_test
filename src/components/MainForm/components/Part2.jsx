import React, { Component } from "react"
import FormService from "../../../services/FormService"

class Part2 extends Component {
    constructor(props) {
        super(props)
        this.formService = new FormService()
        this.state = {
            name: "",
            email: "",
            address: "",
            cp: "",
            city: "",
            state: "",
            country: "",
            siret: "",
            capital: "0",
            countries: null,
        }
    }

    // send form 2 to api
    submitForm = (e) => {
        e.preventDefault()
        const {
            name,
            email,
            address,
            cp,
            city,
            state,
            country,
            siret,
            capital,
        } = this.state

        this.formService.sendStep2({
            name,
            email,
            address,
            cp,
            city,
            state,
            country,
            siret,
            capital,
        })
        this.props.increaseStep()
    }

    async componentDidMount() {
        const countries = await this.formService.getcountriesName()
        this.setState({
            countries,
        })
    }

    changeSelect = (e, field) => {
        this.setState({ [`${field}`]: e.target.value })
    }

    createSelect() {
        const { countries } = this.state
        return countries.map((country) => (
            <option
                value={country.name}
                key={`${country.name}-ctr`}
                className="select__part1__option"
            >
                {country.name}
            </option>
        ))
    }

    render() {
        const {
            name,
            email,
            address,
            cp,
            city,
            state,
            countries,
            siret,
            capital,
        } = this.state
        if (!countries) return <div />
        return (
            <div className="form__content">
                <h3>Créer une entreprise</h3>
                <h2>Informations de l'entreprise</h2>
                <form className="input__container" onSubmit={this.submitForm}>
                    <label className="input__custom__label">
                        Nom légal de l'entreprise*
                        <input
                            type="text"
                            name="name"
                            placeholder="Nom"
                            value={name}
                            onChange={(e) => this.changeSelect(e, "name")}
                            required
                        />
                    </label>
                    <label className="input__custom__label">
                        Email de l'entreprise*
                        <input
                            type="text"
                            name="email"
                            placeholder="Email"
                            value={email}
                            onChange={(e) => this.changeSelect(e, "email")}
                            required
                        />
                    </label>
                    <label className="input__custom__label">
                        Adresse de l'entreprise*
                        <input
                            type="text"
                            name="adresse"
                            placeholder="Adresse"
                            value={address}
                            onChange={(e) => this.changeSelect(e, "address")}
                            required
                        />
                    </label>
                    <div className="infos__address">
                        <label className="input__custom__label input__custom__label--cp">
                            Code postal*
                            <input
                                type="number"
                                name="cp"
                                placeholder="Code"
                                value={cp}
                                onChange={(e) => this.changeSelect(e, "cp")}
                                required
                            />
                        </label>
                        <label className="input__custom__label">
                            Ville*
                            <input
                                type="text"
                                name="city"
                                placeholder="Ville"
                                value={city}
                                onChange={(e) => this.changeSelect(e, "city")}
                                required
                            />
                        </label>
                    </div>
                    <label className="input__custom__label">
                        Région*
                        <input
                            type="text"
                            name="name"
                            placeholder="Région"
                            value={state}
                            onChange={(e) => this.changeSelect(e, "state")}
                            required
                        />
                    </label>
                    <label className="input__custom__label">
                        Pays*
                        <select
                            className="select__part1"
                            onChange={(e) => this.changeSelect(e, "country")}
                            required
                        >
                            {this.createSelect()}
                        </select>
                    </label>
                    <label className="input__custom__label">
                        Siret*
                        <input
                            type="text"
                            name="siret"
                            placeholder="Siret"
                            value={siret}
                            onChange={(e) => this.changeSelect(e, "siret")}
                            required
                        />
                    </label>
                    <label className="input__custom__label">
                        Capital social de l'entreprise*
                        <input
                            type="number"
                            name="siret"
                            placeholder="Capital"
                            value={capital}
                            onChange={(e) => this.changeSelect(e, "capital")}
                            required
                        />
                    </label>
                    <p className="italic">*informations obligatoires</p>
                    <div className="footer__container">
                        <p>REVENIR PLUS TARD</p>
                        <button className="button__next">CONTINUER</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default Part2
