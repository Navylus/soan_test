import React, { Component } from "react"
import FormService from "../../../services/FormService"

class Part1 extends Component {
    constructor(props) {
        super(props)
        this.formService = new FormService()
        this.state = {
            status: "SARL",
        }
    }

    // send status to api
    submitForm = (e) => {
        e.preventDefault()
        const { status: statut } = this.state
        this.formService.sendStatut({ statut })
        this.props.increaseStep()
    }

    createSelect() {
        // get company status from an api or a file (easier to add or remove status from the list)
        const companyStatus = this.formService.getCompanyStatus()
        return companyStatus.map((status) => (
            <option
                value={status}
                key={`${status}-status`}
                className="select__part1__option"
            >
                {status}
            </option>
        ))
    }

    changeSelect = (e) => {
        this.setState({ status: e.target.value })
    }

    render() {
        return (
            <div className="form__content">
                <h3>Créer une entreprise</h3>
                <h2>Quel est le statut de votre entreprise</h2>
                <form className="input__container" onSubmit={this.submitForm}>
                    <select
                        className="select__part1"
                        onChange={this.changeSelect}
                    >
                        {this.createSelect()}
                    </select>
                    <div className="footer__container">
                        <p>REVENIR PLUS TARD</p>
                        <button className="button__next">CONTINUER</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default Part1
