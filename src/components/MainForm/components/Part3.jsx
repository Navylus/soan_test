import React, { Component } from "react"
import FormService from "../../../services/FormService"

class Part3 extends Component {
    constructor(props) {
        super(props)
        this.formService = new FormService()
        this.state = {
            firstname: "",
            name: "",
            email: "",
            birthdate: "",
            cityofbirth: "",
            nationality: "",
            address: "",
            cp: "",
            city: "",
            state: "",
            country: "",
            countries: null,
        }
    }

    // send form 3 to api
    submitForm = (e) => {
        e.preventDefault()
        const {
            firstname,
            name,
            email,
            birthdate,
            cityofbirth,
            nationality,
            address,
            cp,
            city,
            state,
            country,
        } = this.state

        this.formService.sendStep2({
            firstname,
            name,
            email,
            birthdate,
            cityofbirth,
            nationality,
            address,
            cp,
            city,
            state,
            country,
        })
        this.props.increaseStep()
    }

    async componentDidMount() {
        const countries = await this.formService.getcountriesName()
        this.setState({
            countries,
        })
    }

    changeSelect = (e, field) => {
        this.setState({ [`${field}`]: e.target.value })
    }

    createSelect() {
        const { countries } = this.state
        return countries.map((country) => (
            <option
                value={country.name}
                key={`${country.name}-ctr`}
                className="select__part1__option"
            >
                {country.name}
            </option>
        ))
    }

    render() {
        const {
            firstname,
            name,
            email,
            birthdate,
            cityofbirth,
            nationality,
            address,
            cp,
            city,
            state,
            countries,
        } = this.state
        if (!countries) return <div />
        return (
            <div className="form__content">
                <h3>Créer une entreprise</h3>
                <h2>Représentant de l'entreprise</h2>
                <form className="input__container" onSubmit={this.submitForm}>
                    <label className="input__custom__label">
                        Prénom du représentant*
                        <input
                            type="text"
                            name="name"
                            placeholder="Prénom"
                            value={firstname}
                            onChange={(e) => this.changeSelect(e, "firstname")}
                            required
                        />
                    </label>
                    <label className="input__custom__label">
                        Nom du représentant*
                        <input
                            type="text"
                            name="name"
                            placeholder="Nom"
                            value={name}
                            onChange={(e) => this.changeSelect(e, "name")}
                            required
                        />
                    </label>
                    <label className="input__custom__label">
                        Email de l'entreprise*
                        <input
                            type="text"
                            name="email"
                            placeholder="Email"
                            value={email}
                            onChange={(e) => this.changeSelect(e, "email")}
                            required
                        />
                    </label>
                    <div className="infos__address">
                        <label className="input__custom__label input__custom__label--cp">
                            Date de naissance*
                            <input
                                type="date"
                                name="birthdate"
                                value={birthdate}
                                onChange={(e) =>
                                    this.changeSelect(e, "birthdate")
                                }
                                required
                            />
                        </label>
                        <label className="input__custom__label">
                            Ville de naissance*
                            <input
                                type="text"
                                name="cityofbirth"
                                placeholder="Ville"
                                value={cityofbirth}
                                onChange={(e) =>
                                    this.changeSelect(e, "cityofbirth")
                                }
                                required
                            />
                        </label>
                    </div>
                    <label className="input__custom__label">
                        Nationalité du représentant*
                        <input
                            type="text"
                            name="nationality"
                            placeholder="Nationalité"
                            value={nationality}
                            onChange={(e) =>
                                this.changeSelect(e, "nationality")
                            }
                            required
                        />
                    </label>
                    <label className="input__custom__label">
                        Adresse du représentant*
                        <input
                            type="text"
                            name="adresse"
                            placeholder="Adresse"
                            value={address}
                            onChange={(e) => this.changeSelect(e, "address")}
                            required
                        />
                    </label>
                    <div className="infos__address">
                        <label className="input__custom__label input__custom__label--cp">
                            Code postal*
                            <input
                                type="number"
                                name="cp"
                                placeholder="Code"
                                value={cp}
                                onChange={(e) => this.changeSelect(e, "cp")}
                                required
                            />
                        </label>
                        <label className="input__custom__label">
                            Ville*
                            <input
                                type="text"
                                name="city"
                                placeholder="Ville"
                                value={city}
                                onChange={(e) => this.changeSelect(e, "city")}
                                required
                            />
                        </label>
                    </div>
                    <label className="input__custom__label">
                        Région*
                        <input
                            type="text"
                            name="name"
                            placeholder="Région"
                            value={state}
                            onChange={(e) => this.changeSelect(e, "state")}
                            required
                        />
                    </label>
                    <label className="input__custom__label">
                        Pays*
                        <select
                            className="select__part1"
                            onChange={(e) => this.changeSelect(e, "country")}
                            required
                        >
                            {this.createSelect()}
                        </select>
                    </label>
                    <p className="italic">*informations obligatoires</p>
                    <div className="footer__container">
                        <p>REVENIR PLUS TARD</p>
                        <button className="button__next">CONTINUER</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default Part3
