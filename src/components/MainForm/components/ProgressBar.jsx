import React, { Component } from "react"
import "react-step-progress-bar/styles.css"
import { ProgressBar, Step } from "react-step-progress-bar"
import iconCheck from "../../../assets/icon_check.svg"
import iconEdit from "../../../assets/icon_edit_gris.svg"

class ProgressBarCustom extends Component {
    getPercent() {
        const { step } = this.props
        switch (step) {
            case 1:
                return 0
            case 2:
                return 50
            case 3:
                return 100
            case 4:
                return 200
            default:
                return 100
        }
    }

    render() {
        const percent = this.getPercent()
        if (percent === 200) return <div />
        return (
            <div className="progressbar__container">
                <ProgressBar
                    percent={percent}
                    filledBackground="#0076ff"
                    height="2px"
                >
                    <Step transition="scale">
                        {({ accomplished }) => (
                            <img
                                alt="test1"
                                className={`step__content ${
                                    accomplished && percent !== 0
                                        ? "step__content--finished"
                                        : percent === 0
                                        ? "step__content--current"
                                        : "step__content--todo"
                                }`}
                                width="30"
                                style={{}}
                                src={percent === 0 ? iconEdit : iconCheck}
                            />
                        )}
                    </Step>
                    <Step transition="scale">
                        {({ accomplished }) => (
                            <img
                                alt="test2"
                                className={`step__content ${
                                    accomplished && percent !== 50
                                        ? "step__content--finished"
                                        : percent === 50
                                        ? "step__content--current"
                                        : "step__content--todo"
                                }`}
                                width="30"
                                src={percent === 50 ? iconEdit : iconCheck}
                            />
                        )}
                    </Step>
                    <Step transition="scale">
                        {({ accomplished }) => (
                            <img
                                alt="test3"
                                className={`step__content ${
                                    accomplished && percent !== 100
                                        ? "step__content--finished"
                                        : percent === 100
                                        ? "step__content--current"
                                        : "step__content--todo"
                                }`}
                                width="30"
                                src={percent === 100 ? iconEdit : iconCheck}
                            />
                        )}
                    </Step>
                </ProgressBar>
            </div>
        )
    }
}

export default ProgressBarCustom
