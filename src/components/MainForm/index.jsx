import React, { Component } from "react"
import "./style.scss"
import Part1 from "./components/Part1"
import Part2 from "./components/Part2"
import Part3 from "./components/Part3"
import ProgressBarCustom from "./components/ProgressBar"
import Done from "./components/Done/index.jsx"

class MainForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            step: 1,
        }
    }

    increaseStep = () => {
        this.setState({ step: this.state.step + 1 })
    }

    getPart() {
        const { step } = this.state
        switch (step) {
            case 1:
                return <Part1 increaseStep={this.increaseStep} />
            case 2:
                return <Part2 increaseStep={this.increaseStep} />
            case 3:
                return <Part3 increaseStep={this.increaseStep} />
            case 4:
                return <Done />
            default:
                return <Part1 increaseStep={this.increaseStep} />
        }
    }

    render() {
        const { step } = this.state
        return (
            <div className="form__container">
                <ProgressBarCustom step={step} />
                {this.getPart()}
            </div>
        )
    }
}

export default MainForm
